# CS 445 Score Report 

## Nathan Lantz

|   Lab  |  Score  | Comments |
|--------|---------|----------|
|    1   | 100/100 |          |
|    2   | 100/100 |          |
|    3   |  45/150 |You lost 8 points on task2 because I cannot see any screenshots. You lost 6 points on task3 for the same reason. You lost 14 points on the task5 because there is no screenshot to base your conclusion on. You lost 12 points on task6 for the same reason. Another 23 point loss is on task7 where you gave up halfway through step2 and skipped step3 completely. On task8, you did not show the steps to get to your conclusion; hence, you lost 15 points. On task9, you lost 17 points because I can see only your explanation, and it is not clear whether the file /etc/zzz is modified and why it worked that way.|
|    4   |   0/100 | No submission was received.|
|    5   | 114/150 | You lost 20 points on Task2 because you did not show how you get to that screenshot. You lost 6 points on Task3 because you did not use the new shellcode in exploit.c and did not explain your results. You lost 10 points on Task4 because you did not explain as required by the assignment.|
|    6   | 88/100 | You did not try changing the filename from retlib to something else, did not report and explain. Hence you lost 10 points. You lost 2 points on Task2 and Task3 because there are no screenshots to illustrate your explanation.|
|    7   |92.4/200| Your Task1.4 does spoofing while snoofing is what this Task asks for. You lost 5 points on this. You lost 3 points on Task2.1B, capturing tcp packets, because your filter expression is incorrect. You did not do Task2.1C, Task2.2 and Task2.3, which make you lose 60 points. Since this is a late submission, 70% of 132 out of 200 is recorded, which is 92.4.|
|    8   | 100/125| You lost 5 points on Task4 because no scapy script was run in this Task4. You lost 20 points on Task5 because the attack was not carried out properly.|


Final Exam: 52 out of 100

Problem 1: 10 out of 25
    - It needs to have two compilations: one with "-g" and the other without "-g". Use the one with "-g" for gdb to figure out the gap between ebp and buffer: -10 
    - Unsuccessful attacks: -5

Problem 2: 10 out of 25
    - The wireshark screenshot of the package used to generate the attacks is not shown: -5
    - Delete "test" manually on the server, instead of TCP Hijacking attacks!: -10

Problem 3: 12 out of 25
    - Does not create the signature from input.txt file and then verify it: -8
    - Part (5): -5

Problem 4: 20 out of 25
    - It will filter out all ICMP packages, instead of only ICMP echo request packages: -3
    - The message in prink still shows "Telnet", and the file name is still using "telent": -2
